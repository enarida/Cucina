//
//  DessertsTableViewCell.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class DessertsTableViewCell: UITableViewCell {
    
    //Variables
    @IBOutlet var dessertImageView: UIImageView!
    @IBOutlet var dessertFoodName: UILabel!
    @IBOutlet var dessertProfile: UIImageView!
    @IBOutlet var dessertLocation: UILabel!
    
    var dessert: Dessert! {
        didSet{
            
            self.updateUI()
        }
    }
    
    func updateUI() {
        
        // ARRAYS ->
        
        dessertImageView.image = UIImage(named: dessert.imageName)
        dessertFoodName.text = dessert.foodName
        dessertLocation.text = dessert.location
        dessertProfile.image = UIImage(named: dessert.profile)
        
        dessertImageView.layer.masksToBounds = true
        
        
        
    }
    
}