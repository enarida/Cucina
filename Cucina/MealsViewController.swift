//
//  MealsViewController.swift
//  Cucina
//
//  Created by Imergex on 10/14/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class MealsTableViewController: UITableViewController {

    @IBOutlet var mealsTableView: UITableView!
    
    // meals -> [CategoryMeals struct] and [GetallMeals array]
    var meals = CategoryMeals.getAllMeals()
    
    var identities = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mealsTableView.dataSource = self
        
        identities = ["1","2","3","4"]
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //Populating data on the tableView
        
        let cell = tableView.dequeueReusableCellWithIdentifier("mealCell") as! MealsTableViewCell
        
        let meal = meals[indexPath.row]
        
        cell.meal = meal
        
        return cell
    }
    
    //DID SELECTED CELL
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let foodName = identities[indexPath.row]
        let viewController = storyboard?.instantiateViewControllerWithIdentifier(foodName)
        self.navigationController?.pushViewController(viewController!, animated: true)
    }

}

