//
//  DessertsTableViewController.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class DessertsTableViewController: UITableViewController {

    @IBOutlet var dessertsTableView: UITableView!
    
    //meals -> [CategoryMeals struct] and [GetallMeals array]
    var desserts = CategoryDesserts.getAllDesserts()
    
    var identities = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dessertsTableView.dataSource = self
        
        dessertsTableView.estimatedRowHeight = 177
        
        identities = ["10","11","12","13"]
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return desserts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //Populating data on the tableView
        
        let cell = tableView.dequeueReusableCellWithIdentifier("dessertsCell") as! DessertsTableViewCell
        
        let dessert = desserts[indexPath.row]
        
        cell.dessert = dessert
        
        return cell
    }
    
    //DID SELECTED CELL
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let foodName = identities[indexPath.row]
        let viewController = storyboard?.instantiateViewControllerWithIdentifier(foodName);        self.navigationController?.pushViewController(viewController!, animated: true)
    }
}