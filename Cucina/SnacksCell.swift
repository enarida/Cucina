//
//  SnacksCell.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import Foundation

//Creating the layout for the cells
struct Snacks {
    
    var imageName = String()
    var foodName = String()
    var location = String()
    var profile = String()
}

//Creating the data of the cells
struct CategorySnacks {
    
    // the [Meals] <- is the array
    static func getAllSnacks() -> [Snacks] {
        return [
            //dummmy data cell
            
            Snacks(imageName: "burger", foodName: "German Burger", location: "Pasig City, NCR", profile: "tao5"),
            Snacks(imageName: "pizza", foodName: "Pizza", location: "Quezon City, NCR", profile: "tao6"),
            Snacks(imageName: "nachos", foodName: "Nachos", location: "Valenzuela City, NCR", profile: "tao7"),
            Snacks(imageName: "fries", foodName: "Gourmet Fries", location: "Pasig City, NCR", profile: "tao8")
            
        ]
        
    }
    
}
