//
//  SnacksViewController.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class SnacksViewController: UITableViewController {
    
    
    @IBOutlet var snacksTableView: UITableView!
     //meals -> [CategoryMeals struct] and [GetallMeals array]
    var snacks = CategorySnacks.getAllSnacks()
    
    var identities = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        snacksTableView.dataSource = self
        
        snacksTableView.estimatedRowHeight = 177
        
        identities = ["5","6","7","8"]
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return snacks.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //Populating data on the tableView
        
        let cell = tableView.dequeueReusableCellWithIdentifier("snacksCell") as! SnacksTableViewCell
        
        let snack = snacks[indexPath.row]
        
        cell.snack = snack
        
        return cell
    }
    
    //DID SELECTED CELL
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let foodName = identities[indexPath.row]
       let viewController = storyboard?.instantiateViewControllerWithIdentifier(foodName);        self.navigationController?.pushViewController(viewController!, animated: true)
    }
}
