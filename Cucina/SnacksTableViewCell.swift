//
//  SnacksTableViewCell.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class SnacksTableViewCell: UITableViewCell {
    
    //Variables
    @IBOutlet var snackImageView: UIImageView!
    
    @IBOutlet var snackFoodName: UILabel!

    @IBOutlet var snackLocation: UILabel!
    
    @IBOutlet var snackProfile: UIImageView!
    var snack: Snacks! {
        didSet{
            
            self.updateUI()
        }
    }
    
    func updateUI() {
        
        // ARRAYS ->
        
        snackImageView.image = UIImage(named: snack.imageName)
        snackFoodName.text = snack.foodName
        snackLocation.text = snack.location
        snackProfile.image = UIImage(named: snack.profile)
        
        snackImageView.layer.masksToBounds = true
        
        
        
    }
    
}