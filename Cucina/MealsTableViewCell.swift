//
//  MealsTableViewCell.swift
//  Cucina
//
//  Created by Imergex on 10/14/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import UIKit

class MealsTableViewCell: UITableViewCell {

    //Variables
    @IBOutlet var mealImageView: UIImageView!

    @IBOutlet var mealFoodName: UILabel!
    
    @IBOutlet var mealLocation: UILabel!
    
    @IBOutlet var mealProfile: UIImageView!
    
    var meal: Meals! {
        didSet{
            
            self.updateUI()
        }
    }
    
    func updateUI() {
        
        // ARRAYS ->
        
        mealImageView.image = UIImage(named: meal.imageName)
        mealFoodName.text = meal.foodName
        mealLocation.text = meal.location
        mealProfile.image = UIImage(named: meal.profile)
        
        mealImageView.layer.masksToBounds = true
        
        
        
    }
    
    
    
}
