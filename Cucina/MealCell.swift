//
//  MealCell.swift
//  Cucina
//
//  Created by Imergex on 10/14/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import Foundation

//Creating the layout for the cells
struct Meals {
    
    var imageName = String()
    var foodName = String()
    var location = String()
    var profile = String()
}

//Creating the data of the cells
struct CategoryMeals {
    
    // the [Meals] <- is the array
    static func getAllMeals() -> [Meals] {
        return [
            //dummmy data cell
        
        Meals(imageName: "adobo", foodName: "Adobo", location: "Quezon City, NCR", profile: "tao1"),
        Meals(imageName: "sinigang", foodName: "Sinigang", location: "Manila, NCR", profile: "tao2"),
        Meals(imageName: "caldereta", foodName: "Caldereta", location: "Valenzuela City, NCR", profile: "tao3"),
        Meals(imageName: "karekare", foodName: "Kare-Kare", location: "Pasig City, NCR", profile: "tao4")
        
        ]
        
    }
    
}