//
//  DessertCell.swift
//  Cucina
//
//  Created by Imergex on 10/15/16.
//  Copyright © 2016 Imergex. All rights reserved.
//

import Foundation

//Creating the layout for the cells
struct Dessert {
    
    var imageName = String()
    var foodName = String()
    var location = String()
    var profile = String()
}

//Creating the data of the cells
struct CategoryDesserts {
    
    // the [Meals] <- is the array
    static func getAllDesserts() -> [Dessert] {
        return [
            //dummmy data cell
            
            Dessert(imageName: "lecheflan", foodName: "Leche Flan", location: "Pasig City, NCR", profile: "tao9"),
            Dessert(imageName: "cannoli", foodName: "Cannoli", location: "Caloocan City, NCR", profile: "tao10"),
            Dessert(imageName: "rownies", foodName: "Brownies", location: "Valenzuela City, NCR", profile: "tao11"),
            Dessert(imageName: "waffle", foodName: "Waffle", location: "Quezon City, NCR", profile: "tao13")
            
        ]
        
    }
    
}

